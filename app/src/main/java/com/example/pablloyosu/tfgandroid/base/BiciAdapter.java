package com.example.pablloyosu.tfgandroid.base;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.pablloyosu.tfgandroid.R;
import com.example.pablloyosu.tfgandroid.modelos.Bicicleta;

import java.util.ArrayList;

public class BiciAdapter extends BaseAdapter{

    private Bicicleta[] bicicletas;
    LayoutInflater inflater;
    Context context;

    public BiciAdapter(Bicicleta[] bicicletas, Context context) {
        this.bicicletas = bicicletas;
        this.inflater = LayoutInflater.from(context);
        this.context = context;
    }


    @Override
    public int getCount() {
        return bicicletas.length;
    }

    @Override
    public Object getItem(int i) {
        return bicicletas[i];
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    //Rellenar datos de bicicleta
    @Override
    public View getView(int posicion, View view, ViewGroup viewGroup) {
        ViewHolder viewHolder;
        if (view==null){
            view = inflater.inflate(R.layout.adapter_bicicleta, null);
            viewHolder = new ViewHolder();
            viewHolder.ivAdBici = view.findViewById(R.id.ivAdBicicleta);
            viewHolder.tvInfoDisponibilidad = view.findViewById(R.id.tvInfoDispoibilidad);
            viewHolder.tvInfoEstado = view.findViewById(R.id.tvInfoEstado);
            viewHolder.tvInfoBateria = view.findViewById(R.id.tvInfoBateria);
            view.setTag(viewHolder);
        }else {
            viewHolder = (ViewHolder) view.getTag();
        }
        Bicicleta b = bicicletas[posicion];
        viewHolder.tvInfoDisponibilidad.setText(b.getEstacion().getCalle());
        viewHolder.tvInfoEstado.setText(b.getEstadoEnum());
        viewHolder.tvInfoBateria.setText(b.getBateria() + "%");

        return view;
    }


    private class ViewHolder {
        ImageView ivAdBici;
        TextView tvInfoDisponibilidad;
        TextView tvInfoEstado;
        TextView tvInfoBateria;
    }
}
