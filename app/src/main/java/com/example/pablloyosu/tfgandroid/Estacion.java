package com.example.pablloyosu.tfgandroid;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.example.pablloyosu.tfgandroid.Interfaces.OnTaskCompleteListener;
import com.example.pablloyosu.tfgandroid.base.Sesion;
import com.example.pablloyosu.tfgandroid.modelos.EstacionBici;
import com.example.pablloyosu.tfgandroid.task.EstacionCrearTask;

import static com.example.pablloyosu.tfgandroid.R.string.ok;

//Código del layout estación
public class Estacion extends AppCompatActivity implements View.OnClickListener, OnTaskCompleteListener
       //, CompoundButton.OnCheckedChangeListener
{
    Intent intent;
    private EditText etCalle, etLatitud, etLongitud, etAnlclajes, etBicisDisponibles, etCapacidad;
    private CheckBox cbEstacionAbierta;
    private Button btOk, btCancelar;
    private EstacionBici estacionBici;
    private boolean isCreation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_estacion);
        bindUI();
        addOnClickListener(this);
        intent = getIntent();

        //Determinar opciones que vamos a hacer segun el intent que nos llegue
        if(intent.getBooleanExtra("CREAR", false)){
            isCreation = true;

        }else if(intent.getBooleanExtra("UPDATE", false)){
            isCreation = false;

        }else if(intent.getBooleanExtra("DELETE", false)){
            isCreation = false;
        }
        if(intent.getSerializableExtra("object") !=null){
            estacionBici = (EstacionBici) intent.getSerializableExtra("object");
            if(intent.getDoubleArrayExtra("COORDS")!= null){
                double[] coord = intent.getDoubleArrayExtra("COORDS");
                System.out.println("----------------" + coord[0]);
                System.out.println("----------------" + coord[1]);
                estacionBici.setLat(coord[0]);
                estacionBici.setLng(coord[1]);
            }

            setParams(isCreation);
        }


    }

    private void bindUI(){
        System.out.println("-------------------------> bindUI" );
        etCalle = findViewById(R.id.etCalle);
        etLatitud = findViewById(R.id.etLatitud);
        etLongitud = findViewById(R.id.etLongitud);
        etAnlclajes = findViewById(R.id.etAnclajesDisponibles);
        etBicisDisponibles = findViewById(R.id.etBicisDisponibles);
        etCapacidad = findViewById(R.id.etCapacidad);
        cbEstacionAbierta = findViewById(R.id.cbAbierto);
        btOk = findViewById(R.id.btOk);
        btCancelar = findViewById(R.id.btCancel);

    }

    //Añadir listeners
    private void addOnClickListener(View.OnClickListener listener){
        cbEstacionAbierta.setOnClickListener(listener);
        btOk.setOnClickListener(listener);
        btCancelar.setOnClickListener(listener);
        cbEstacionAbierta.setOnClickListener(listener);
    }

    //Rellenar modelo Estación con los datos del layout
    private void setParams(boolean isCreation){
        System.out.println("-------------------------> setParams" );
        if(isCreation){
            etLatitud.setText(estacionBici.getLat().toString());
            etLongitud.setText(estacionBici.getLng().toString());
        }else {
            etCalle.setText(estacionBici.getCalle());
            etLatitud.setText(String.valueOf(estacionBici.getLat()));
            etLongitud.setText(String.valueOf(estacionBici.getLng()));
            etAnlclajes.setText(String.valueOf(estacionBici.getAnclajesDisponibles()));
            etBicisDisponibles.setText(String.valueOf(estacionBici.getBicisDisponibles()));
            etCapacidad.setText(String.valueOf(estacionBici.getCapacidad()));
            cbEstacionAbierta.setChecked(estacionBici.isEstacionAbierta());
            if(cbEstacionAbierta.isChecked()){
                cbEstacionAbierta.setText(R.string.marcar_cerrado);
            }else {
                cbEstacionAbierta.setText(R.string.marcar_abierto);
            }
        }
    }

    //Rellenar el layout con los datos del modelo
    private void getParams(){
        estacionBici.setCalle(String.valueOf(etCalle.getText()));
        estacionBici.setLat(Double.parseDouble(String.valueOf(etLatitud.getText())));
        estacionBici.setLng(Double.parseDouble(String.valueOf(etLongitud.getText())));
        estacionBici.setAnclajesDisponibles(Integer.parseInt(String.valueOf(etAnlclajes.getText())));
        estacionBici.setBicisDisponibles(Integer.parseInt(String.valueOf(etBicisDisponibles.getText())));
        estacionBici.setCapacidad(Integer.parseInt(String.valueOf(etCapacidad.getText())));
        estacionBici.setEstacionAbierta(cbEstacionAbierta.isChecked());
        System.out.println(cbEstacionAbierta.isChecked());
        EstacionCrearTask estacionCrearTask;
        if(isCreation){

            estacionCrearTask = new EstacionCrearTask(estacionBici, this, "POST");
            estacionCrearTask.execute();
        }else {

            estacionCrearTask = new EstacionCrearTask(estacionBici, this, "PUT");
            estacionCrearTask.execute();
        }

    }

    private void setEditable(Boolean estado){
        etCalle.setEnabled(estado);
        etLatitud.setEnabled(estado);
        etLongitud.setEnabled(estado);
        etAnlclajes.setEnabled(estado);
        etBicisDisponibles.setEnabled(estado);
        etCapacidad.setEnabled(estado);
        cbEstacionAbierta.setEnabled(estado);
    }

    //Acciones al clickar en objetos
    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btOk:
                if(Sesion.usuario.isAdmin()){
                    if(getResources().getString(R.string.ok).toString()
                            .equals(btOk.getText().toString())){
                        //add
                        getParams();
                        System.out.println(estacionBici.toString());
                    }else if (getResources().getString(R.string.guardar_cambios).toString()
                            .equals(btOk.getText().toString())){
                        //update
                        getParams();
                    }else if(getResources().getString(R.string.borrar)
                            .toString().equals(btOk.getText().toString())) {
                        //delete
                    }

                }else {
                    Toast.makeText(this, "Tu Usuario no tiene privilegios para modificar datos",
                            Toast.LENGTH_LONG).show();
                    Intent intent = new Intent(this, Mapa.MainMenu.class);
                    startActivity(intent);
                }

                break;
            case R.id.btCancel:
                Intent intent = new Intent(this, Mapa.MainMenu.class);
                startActivity(intent);
                break;
            case R.id.cbAbierto:
                if(cbEstacionAbierta.isChecked()){
                    cbEstacionAbierta.setText(R.string.marcar_cerrado);
                }else {
                    cbEstacionAbierta.setText(R.string.marcar_abierto);
                }
                break;
        }
    }

    //Al finalizar la tarea
    @Override
    public void onTaskCompleted(Object object) {
        if(object != null && object instanceof EstacionBici){
            Toast.makeText(this, "Estación creada", Toast.LENGTH_LONG).show();
            Intent intent = new Intent(this, Mapa.MainMenu.class);
            startActivity(intent);
        }else {
            Toast.makeText(this, "Error", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onTaskCompletedList(Object[] object) {

    }


    //Cuando los cambios son ajenos al Usuario utilizar este evento
    /*@Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
        System.out.println("listener "+ b);
        if(cbEstacionAbierta.isChecked()){
            cbEstacionAbierta.setText(R.string.marcar_cerrado);
        }else {
            cbEstacionAbierta.setText(R.string.marcar_abierto);
        }

    }*/
}
