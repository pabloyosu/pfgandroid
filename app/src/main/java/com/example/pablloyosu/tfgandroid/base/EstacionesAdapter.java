package com.example.pablloyosu.tfgandroid.base;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.pablloyosu.tfgandroid.R;
import com.example.pablloyosu.tfgandroid.modelos.EstacionBici;

import java.util.ArrayList;

public class EstacionesAdapter extends BaseAdapter{
    EstacionBici[] estacionBicis;
    Context context;
    LayoutInflater inflater;

    public EstacionesAdapter(EstacionBici[] estacionBicis , Context context) {
        this.estacionBicis = estacionBicis;
        this.context = context;
        this.inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        //Devuelves el total de objetos a pintar
        return estacionBicis.length;
    }

    @Override
    public Object getItem(int i) {
        return estacionBicis[i];
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    //Rellenar datos estacion
    @Override
    public View getView(int posicion, View view, ViewGroup viewGroup) {
        ViewHolder viewHolder;
        if (view==null){
            view = inflater.inflate(R.layout.adapter_estaciones, null);
            viewHolder = new ViewHolder();
            viewHolder.ivAdEstacion = view.findViewById(R.id.ivAdEstUbicacion);
            viewHolder.tvAdEstDispoible = view.findViewById(R.id.tvAdEstDisponibilidadRes);
            viewHolder.tvAdBicisDisponibles = view.findViewById(R.id.tvAdEstBicisDisponiblesRes);
            viewHolder.tvAdEstCapacidad = view.findViewById(R.id.tvAdEstCapacidadRes);
            viewHolder.tvAdEstNombre = view.findViewById(R.id.tvAdEstCalle);
            viewHolder.tvAdEstHuecosVacios = view.findViewById(R.id.tvAdEstHuecosVaciosRes);
            view.setTag(viewHolder);
        }else {
            viewHolder = (ViewHolder) view.getTag();
        }
        EstacionBici eb = estacionBicis[posicion];
        System.out.println( eb.toString());
        viewHolder.tvAdEstDispoible.setText(eb.getEstado());
        /**
         * Lo correcto sería declarar un unico StringBuilder y limpiarlo
         */
        viewHolder.tvAdEstNombre.setText(eb.getCalle());
        viewHolder.tvAdEstDispoible.setText(eb.getEstado());
        viewHolder.tvAdEstCapacidad.setText(String.valueOf(eb.getCapacidad()));
        viewHolder.tvAdBicisDisponibles.setText(String.valueOf(eb.getBicisDisponibles()));
        viewHolder.tvAdEstHuecosVacios.setText(String.valueOf(eb.getAnclajesDisponibles()));
        viewHolder.ivAdEstacion.setImageResource(android.R.drawable.ic_dialog_info);


        return view;
    }


    private class ViewHolder {
        ImageView ivAdEstacion;
        TextView tvAdEstDispoible;
        TextView tvAdBicisDisponibles;
        TextView tvAdEstCapacidad;
        TextView tvAdEstHuecosVacios;
        TextView tvAdEstNombre;

    }
}
