package com.example.pablloyosu.tfgandroid.base;

import android.text.TextUtils;
import android.util.Patterns;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import uk.me.jstott.jcoord.LatLng;
import uk.me.jstott.jcoord.UTMRef;

//Clase con métodos útiles
public class Util {

    //Comprobación email tiene parámetros correctos
    public static boolean isValidEmail(String email){
        return !TextUtils.isEmpty(email) && Patterns.EMAIL_ADDRESS.matcher(email).matches();

    }

    //Comprobación contraseña tiene parámetros correctos
    public static boolean isValidPassword(String password){
        return password.length() >= 4;

    }

    //Métodos puestos para intentar corregir el problema de coordenadas
    public static void cambiarCoordenadas(double lat, double lng){

    }
    public static LatLng parseCoordenadas(double latitud, double longitud){
        UTMRef utm = new UTMRef(latitud, longitud, 'N', 30);

        return utm.toLatLng();
    }


    public static String getStringFromInputStream(InputStream is) throws IOException { BufferedReader br = new BufferedReader(new
            InputStreamReader(is));
        StringBuilder total = new StringBuilder(); String line;
        while ((line = br.readLine()) != null) {
            total.append(line).append('\n'); }
        return total.toString(); }

    public static String fromJson (String linea){
        return linea.replaceAll("latitud","lat").replaceAll("longitud", "lng");
    }

    public static String toJsom (String linea){
        return linea.replaceAll("lat", "latitud").replaceAll("lng", "longitud");
    }
}
