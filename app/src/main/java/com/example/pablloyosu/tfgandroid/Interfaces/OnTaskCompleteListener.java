package com.example.pablloyosu.tfgandroid.Interfaces;

public interface OnTaskCompleteListener {

     void onTaskCompleted(Object object);
     void onTaskCompletedList(Object[] object);
}
