package com.example.pablloyosu.tfgandroid.task;

import android.os.AsyncTask;

import com.example.pablloyosu.tfgandroid.Interfaces.OnTaskCompleteListener;
import com.example.pablloyosu.tfgandroid.base.Util;
import com.example.pablloyosu.tfgandroid.modelos.Bicicleta;
import com.example.pablloyosu.tfgandroid.modelos.Usuario;
import com.google.gson.Gson;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

import static com.example.pablloyosu.tfgandroid.base.Constantes.URL_SPRING;

//Tarea obtener bicicletas
public class BicicletasGetTask extends AsyncTask<Void, Void, Bicicleta[]>{
    private OnTaskCompleteListener listener;
    private Bicicleta []bicicleta;
    public BicicletasGetTask(OnTaskCompleteListener listener) {
        this.listener = listener;
    }

    //endPoint
    @Override
    protected Bicicleta[] doInBackground(Void... voids) {
        try {
            URL url = new URL(URL_SPRING + "/get_bicicletas");
            HttpURLConnection urlConnection ;
            urlConnection = ((HttpURLConnection) url.openConnection());
            urlConnection.setRequestProperty("Content-Type","application/json");

            Gson gson = new Gson();


            InputStream in = new BufferedInputStream(urlConnection.getInputStream());
            String linea = Util.getStringFromInputStream(in);
            System.out.println("_______________\n"+linea+"\n---------------------");

            bicicleta = gson.fromJson(Util.fromJson(linea), Bicicleta[].class);
            for (Bicicleta f:
                 bicicleta) {
                System.out.println(f.toString());
            }
            return bicicleta;
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(Bicicleta[] bicicletas) {
        super.onPostExecute(bicicletas);
        listener.onTaskCompletedList(bicicletas);
    }
}
