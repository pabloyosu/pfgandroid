package com.example.pablloyosu.tfgandroid;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.pablloyosu.tfgandroid.modelos.EstacionBici;

//Código layout incidencia
public class Incidencia extends AppCompatActivity implements View.OnClickListener{
    private EditText etNombre;
    private EditText etLatitud;
    private EditText etLongitud;
    private EditText etDescripcion;
    private Button btEnviar;
    private Button btCancelar;
    private String msg = "%%% situada en ### tiene una avería: ";
    EstacionBici estacionBici;

    Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_incidencia);
        bindUI();
        addOnClickListener(this);
        intent = getIntent();
        if(intent.getSerializableExtra("object") instanceof EstacionBici){
            estacionBici = (EstacionBici) intent.getSerializableExtra("object");
            msg = msg
                    .replaceAll("%%%", "La estación ")
                    .replaceAll("###", estacionBici.getCalle());

            setParams();
        }else if(intent.getDoubleArrayExtra("COORDS") !=null){

        }
    }

    private void bindUI(){
        etNombre = findViewById(R.id.etNombreIncidencia);
        etLatitud = findViewById(R.id.etLatitud);
        etLongitud = findViewById(R.id.etLongitud);
        etDescripcion = findViewById(R.id.etDescripcion);
        btEnviar = findViewById(R.id.btEnviar);
        btCancelar = findViewById(R.id.btCancelar);
    }

    private void addOnClickListener(View.OnClickListener listener){
        btEnviar.setOnClickListener(listener);
        btCancelar.setOnClickListener(listener);
    }

    //Rellenar las lat y lng automaticamente
    private void setParams(){
        etLatitud.setText(String.valueOf(estacionBici.getLat()));
        etLongitud.setText(String.valueOf(estacionBici.getLng()));
        etDescripcion.setText(msg);
    }

    private void getParams(){
        Intent intent = new Intent(this, Mapa.MainMenu.class);
        startActivity(intent);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btEnviar:
                //getParams() sin almacenar incidencias
                getParams();
                break;
            case R.id.btCancelar:
                Intent intent = new Intent(this, Mapa.MainMenu.class);
                startActivity(intent);

                break;
        }

    }


}
