package com.example.pablloyosu.tfgandroid;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.pablloyosu.tfgandroid.Interfaces.OnTaskCompleteListener;
import com.example.pablloyosu.tfgandroid.base.Sesion;
import com.example.pablloyosu.tfgandroid.task.RegistroTask;

//Código del layout bicicleta
public class Usuario extends AppCompatActivity implements View.OnClickListener, AdapterView.OnItemSelectedListener, OnTaskCompleteListener {
    Intent intent;
    private EditText etNombre;
    private EditText etApellidos;
    private EditText etEmail;
    private EditText etPaypal;
    private TextView tvMostrarSaldo;
    private Button btOk;
    private Button btCancelar;
    private com.example.pablloyosu.tfgandroid.modelos.Usuario usuario;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_usuario);
        bindUI();

        //Listener botones
        setOnItemClickListener(this);

        intent = getIntent();

        //Obtener usuario de la sesión
        usuario = Sesion.usuario;

        //Poner datos del usuario de la sesion
        setParams(usuario);
    }

    private void setParams(com.example.pablloyosu.tfgandroid.modelos.Usuario usuarioActual) {
        System.out.println("---------------> setPArams Usuario");
        etNombre.setText(usuarioActual.getNombre());
        etApellidos.setText(usuarioActual.getApellidos());
        etEmail.setText(usuarioActual.getEmail());
        etPaypal.setText(usuarioActual.getPaypal());
        tvMostrarSaldo.setText(String.valueOf(usuarioActual.getSaldo()));

    }

    private void setOnItemClickListener(View.OnClickListener listener) {
        btOk.setOnClickListener(listener);
        btCancelar.setOnClickListener(listener);
    }

    private void bindUI() {
        etNombre =  findViewById(R.id.etUsuarioNombre);
        etApellidos = findViewById(R.id.etUsuarioApellidos);
        etEmail = findViewById(R.id.etUsuarioEmail);
        etPaypal = findViewById(R.id.etUsuarioPaypal);
        tvMostrarSaldo = findViewById(R.id.tvMostrarSaldo);
        btOk = findViewById(R.id.btUsuarioOk);
        btCancelar = findViewById(R.id.btUsuarioCancelar);
    }

    @Override
    public void onClick(View view) {
        System.out.println("-----------BOTON PULSADO-----------");
        switch (view.getId()){
            case R.id.btUsuarioOk:
                getParams();
                System.out.println(usuario.toString());
                intent = new Intent(this, Mapa.MainMenu.class);
                startActivity(intent);
                break;
                
            case R.id.btUsuarioCancelar:
                intent = new Intent(this, Mapa.MainMenu.class);
                startActivity(intent);
                break;
        }

    }

    private void getParams() {
        usuario.setNombre(String.valueOf(etNombre.getText()));
        usuario.setApellidos(String.valueOf(etApellidos.getText()));
        usuario.setEmail(String.valueOf(etEmail.getText()));
        usuario.setPaypal(String.valueOf(etPaypal.getText()));

        System.out.println("PASWORD SESION ----> " + Sesion.usuario.getContrasenia());
        usuario.setContrasenia(Sesion.usuario.getContrasenia());
        System.out.println("PASSWORD ALMACENADA ------> " + usuario.getContrasenia());
        System.out.println("SALDO SESION ----> " + Sesion.usuario.getSaldo());
        usuario.setSaldo(Sesion.usuario.getSaldo());
        System.out.println("SALDO ALMACENADO ----> " + usuario.getContrasenia());
        System.out.println("ADMIN SESION ----> " + Sesion.usuario.isAdmin());
        usuario.setAdmin(Sesion.usuario.isAdmin());
        System.out.println("ADMIN ALMACENADO ----> " + usuario.getContrasenia());

        RegistroTask task;
        task = new RegistroTask(usuario, this, "PUT");
        task.execute();

    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    @Override
    public void onTaskCompleted(Object object) {

    }

    @Override
    public void onTaskCompletedList(Object[] object) {

    }
}
