package com.example.pablloyosu.tfgandroid.modelos;

import java.io.Serializable;

public class ModeloIncidencia implements Serializable{
    private int id;
    private String nombre;
    private Double latitud;
    private Double longitud;
    private String descripcion;
    private int idBicicleta;
    private int idEstacion;
}
