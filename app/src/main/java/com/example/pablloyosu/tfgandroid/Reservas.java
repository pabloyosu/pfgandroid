package com.example.pablloyosu.tfgandroid;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.pablloyosu.tfgandroid.modelos.Bicicleta;

//Clase sin usar
public class Reservas extends AppCompatActivity implements View.OnClickListener{
    private TextView tvEstado;
    private TextView tvBateria;
    private ProgressBar pgBateria;
    private Button btOk;
    private Button btCancelar;
    Intent intent;
    Bicicleta bicicleta;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reservas);
        bindUI();
        setOnClickListener(this);
        intent =  getIntent();
        if(intent.getSerializableExtra("BICI")instanceof Bicicleta) {
            bicicleta = (Bicicleta) intent.getSerializableExtra("BICI");
        }
        setParams();
    }

    private void bindUI(){
        tvEstado = findViewById(R.id.tvEstado);
        tvBateria = findViewById(R.id.tvBateria);
        pgBateria = findViewById(R.id.pgBateria);
        btOk = findViewById(R.id.btOk);
        btCancelar = findViewById(R.id.btCancel);

    }

    private void setOnClickListener(View.OnClickListener listener){
        btOk.setOnClickListener(listener);
        btCancelar.setOnClickListener(listener);
    }

    
    private void setParams(){
        tvEstado.setText(bicicleta.getEstado());
        tvBateria.setText(tvBateria.getText() + ": " + bicicleta.getBateria());
        pgBateria.setProgress(bicicleta.getBateria());

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){

        }
    }
}
