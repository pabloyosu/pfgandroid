package com.example.pablloyosu.tfgandroid;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.RadioButton;

import com.example.pablloyosu.tfgandroid.base.Sesion;
import com.mapbox.mapboxsdk.constants.Style;

//Código de menu preferencias
public class Preferencias extends AppCompatActivity implements View.OnClickListener{
    private RadioButton rbDark;
    private RadioButton rbClear;
    private RadioButton rbLigth;
    private RadioButton rbOrdenarId;
    private RadioButton rbOrdenarAlfabeticamente;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_preferencias);
        bindUI();
        setOnClickListener(this);
        loadPreferences();

    }

    private void bindUI(){
        rbDark = findViewById(R.id.rbDark);
        rbClear = findViewById(R.id.rbClear);
        rbLigth = findViewById(R.id.rbLigth);
        rbOrdenarId = findViewById(R.id.rbOrdenarId);
        rbOrdenarAlfabeticamente = findViewById(R.id.rbOrdenarAlfabeticamente);

    }
    private void setOnClickListener (View.OnClickListener listener){
        rbDark.setOnClickListener(listener);
        rbClear.setOnClickListener(listener);
        rbLigth.setOnClickListener(listener);
        rbOrdenarId.setOnClickListener(listener);
        rbOrdenarAlfabeticamente.setOnClickListener(listener);

    }

    private void loadPreferences(){
        //Opciones aspecto
        switch (Sesion.aspecto){
            case Style.DARK:
                rbDark.setChecked(true);
                break;
            case Style.OUTDOORS:
                rbClear.setChecked(true);
                break;
            case Style.LIGHT:
                rbLigth.setChecked(true);
                break;

            default:
                rbLigth.setChecked(true);
                Sesion.aspecto = Style.LIGHT;
                break;
        }

        //Opciones ordenación
        switch (Sesion.ordenacion){
            case "id":
                rbOrdenarId.setChecked(true);
                break;
            case "alf":
                rbOrdenarAlfabeticamente.setChecked(true);
                break;
            default:
                rbOrdenarId.setChecked(true);
                Sesion.ordenacion = "id";
                break;
        }
    }

    //Items de selección
    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.rbDark:
                Sesion.aspecto = Style.DARK;
                break;
            case R.id.rbClear:
                Sesion.aspecto = Style.OUTDOORS;
                break;
            case R.id.rbLigth:
                Sesion.aspecto = Style.LIGHT;
                break;
            case R.id.rbOrdenarId:
                Sesion.ordenacion = "id";
                break;
            case R.id.rbOrdenarAlfabeticamente:
                Sesion.ordenacion = "alf";
                break;
        }
    }
}
