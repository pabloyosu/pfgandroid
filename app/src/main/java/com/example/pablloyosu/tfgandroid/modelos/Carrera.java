package com.example.pablloyosu.tfgandroid.modelos;

import java.io.Serializable;
import java.util.Date;

//Modelo carrera
public class Carrera implements Serializable{

    private int id;
    private String fecha;
    private String inicio;
    private String fnl;
    private int duracion;
    private float precio;
    private Usuario usuario;
    private Bicicleta bicicleta;
    //private int idUsuario;
    //private int idBici;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getInicio() {
        return inicio;
    }

    public void setInicio(String inicio) {
        this.inicio = inicio;
    }

    public String getFnl() {
        return fnl;
    }

    public void setFnl(String fnl) {
        this.fnl = fnl;
    }

    public int getDuracion() {
        return duracion;
    }

    public void setDuracion(int duracion) {
        this.duracion = duracion;
    }

    public float getPrecio() {
        return precio;
    }

    public void setPrecio(float precio) {
        this.precio = precio;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public Bicicleta getBicicleta() {
        return bicicleta;
    }

    public void setBicicleta(Bicicleta bicicleta) {
        this.bicicleta = bicicleta;
    }

    @Override
    public String toString() {
        return "Carrera{" +
                "id=" + id +

                ", duracion=" + duracion +
                ", precio=" + precio +
                ", Usuario=" + usuario +
                ", bicicleta=" + bicicleta +
                '}';
    }
}
