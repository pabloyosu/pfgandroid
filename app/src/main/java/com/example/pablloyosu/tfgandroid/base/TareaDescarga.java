package com.example.pablloyosu.tfgandroid.base;

import android.content.Context;
import android.os.AsyncTask;
import android.widget.Adapter;

import com.example.pablloyosu.tfgandroid.Interfaces.OnTaskCompleteListener;
import com.example.pablloyosu.tfgandroid.modelos.EstacionBici;
import com.example.pablloyosu.tfgandroid.task.EstacionCrearTask;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.http.HttpAuthentication;
import org.springframework.http.HttpBasicAuthentication;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

import uk.me.jstott.jcoord.LatLng;

import static com.example.pablloyosu.tfgandroid.base.Constantes.URL_AYUNTAMIENTO;

public class TareaDescarga extends AsyncTask<Void, Void, Void> implements OnTaskCompleteListener{

    private ArrayList<EstacionBici> estacionBicis;
    private Context context;
    private EstacionesAdapter estacionesAdapter;

    public TareaDescarga( Context context, EstacionesAdapter estacionesAdapter) {
        this.estacionBicis = new ArrayList<>();
        this.context = context;
        this.estacionesAdapter = estacionesAdapter;
    }

    //Limpiar estacionBicis antes de lanzar la tarea
    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        estacionBicis.clear();
    }

    //Tarea de descargar los datos de la url del ayuntamiento
    @Override
    protected Void doInBackground(Void... voids) {
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());

        String url1 = null;
        InputStream is = null;
        String resultado = null;
        JSONObject json = null;
        JSONArray jsonArray = null;

        try{

            //Preparar conexión
            URL url = new URL(URL_AYUNTAMIENTO);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            StringBuilder stringBuilder = new StringBuilder();
            String linea = null;

            //Leer datos si la linea contiene datos
            while ((linea = bufferedReader.readLine()) != null){
                stringBuilder.append(linea);
            }

            //Desconexión de la url
            connection.disconnect();
            resultado = stringBuilder.toString();
            json = new JSONObject(resultado);
            jsonArray = json.getJSONArray("result");

            //Preparar la introducción de datos en estacionBici
            int id;
            String titulo;
            String estacionAbierta = null;
            int capacidad;
            int anclajesDisponibles;
            String[] coordenadas;
            Double lat;
            Double lng;
            //Recorrer datos almacenados y guardarlos en estacionBici
            EstacionBici estacionBici;
            for (int i = 0; i<jsonArray.length(); i++){
                id = Integer.parseInt(jsonArray.getJSONObject(i).getString("id"));
                titulo = jsonArray.getJSONObject(i).getString("title");
                capacidad = Integer.parseInt(jsonArray.getJSONObject(i).getString("plazas"));
                estacionAbierta = jsonArray.getJSONObject(i).getString("tipo");
                anclajesDisponibles = Integer.parseInt(jsonArray.getJSONObject(i).getString("anclajes"));
                coordenadas = jsonArray.getJSONObject(i).getJSONObject("geometry").getString("coordinates").replaceAll("\\[|\\]", "").split(",");
                lng = Double.valueOf(coordenadas[0].trim());
                lat = Double.valueOf(coordenadas[1].trim());

                estacionBici = new EstacionBici();
                estacionBici.setId(id);
                estacionBici.setCalle(titulo);
                if(estacionAbierta.equalsIgnoreCase("abierto"))
                    estacionBici.setEstacionAbierta(true);
                else
                    estacionBici.setEstacionAbierta(false);

                estacionBici.setCapacidad(capacidad);
                estacionBici.setAnclajesDisponibles(anclajesDisponibles);
                estacionBici.setLat(lat);
                estacionBici.setLng(lng);
                System.out.println("ESTACION---------->"+estacionBici.toString());
                estacionBicis.add(estacionBici);

            }
            for (EstacionBici e: estacionBicis) {
                EstacionCrearTask task = new EstacionCrearTask(e,this, "POST");
                task.execute();
            }


        }catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (HttpClientErrorException e ){
            e.printStackTrace();
        }

        return null;
    }

    @Override
    protected void onProgressUpdate(Void... values) {
        super.onProgressUpdate(values);
    }

    //Notificar cambios si los ha habido
    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
        if(estacionesAdapter !=null){
        estacionesAdapter.notifyDataSetChanged();
        }

    }

    @Override
    public void onTaskCompleted(Object object) {

    }

    @Override
    public void onTaskCompletedList(Object[] object) {

    }
}
