package com.example.pablloyosu.tfgandroid.task;

import android.os.AsyncTask;

import com.example.pablloyosu.tfgandroid.Interfaces.OnTaskCompleteListener;
import com.example.pablloyosu.tfgandroid.base.Util;
import com.example.pablloyosu.tfgandroid.modelos.Bicicleta;
import com.example.pablloyosu.tfgandroid.modelos.EstacionBici;
import com.google.gson.Gson;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;

import static com.example.pablloyosu.tfgandroid.base.Constantes.URL_SPRING;

//Tarea crear bicicleta
public class BicicletasCrearTask extends AsyncTask<Void, Void, Bicicleta> {
    private Bicicleta bicicleta;
    private OnTaskCompleteListener listener;
    private String tipo;

    public BicicletasCrearTask(Bicicleta bicicleta, OnTaskCompleteListener listener, String tipo) {
        this.bicicleta = bicicleta;
        this.listener = listener;
        this.tipo = tipo;
    }

    //endPoint
    @Override
    protected Bicicleta doInBackground(Void... voids) {
        try {
            String endPoint;
            System.out.println("dddddd---->"+bicicleta.toString());
            if (tipo == "DELETE"){
                endPoint= "/bicicletas?id="+bicicleta.getId();
            }else{
                endPoint= "/bicicletas";

            }
            URL url = new URL(URL_SPRING + endPoint);
            HttpURLConnection urlConnection ;
            urlConnection = ((HttpURLConnection) url.openConnection());
            urlConnection.setRequestMethod(tipo);
            urlConnection.setDoOutput(true);
            urlConnection.setRequestProperty("Content-Type","application/json");

            Gson gson = new Gson();


            String plainJson = gson.toJson(bicicleta);
            String sendOb =Util.toJsom(plainJson);
            urlConnection.setChunkedStreamingMode(0);
            System.out.println("plainjson->"+sendOb);
            OutputStream outputStream = new BufferedOutputStream(urlConnection.getOutputStream());

            outputStream.write(sendOb.getBytes());
            outputStream.flush();
            outputStream.close();


            InputStream in = new BufferedInputStream(urlConnection.getInputStream());
            String linea = Util.getStringFromInputStream(in);
            System.out.println("_______________\n"+linea+"\n---------------------");

            bicicleta = gson.fromJson(Util.fromJson(linea), Bicicleta.class);
            System.out.println("_______________\n"+bicicleta.toString()+"\n---------------------");

            return bicicleta;
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (ProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e){
            listener.onTaskCompleted(null);
        }
        return null;
    }

    @Override
    protected void onPostExecute(Bicicleta bicicleta) {
        super.onPostExecute(bicicleta);
        listener.onTaskCompleted(bicicleta);
    }
}
