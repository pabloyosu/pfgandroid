package com.example.pablloyosu.tfgandroid.task;

import android.os.AsyncTask;

import com.example.pablloyosu.tfgandroid.Interfaces.OnTaskCompleteListener;
import com.example.pablloyosu.tfgandroid.base.Sesion;
import com.example.pablloyosu.tfgandroid.base.Util;
import com.example.pablloyosu.tfgandroid.modelos.Bicicleta;
import com.example.pablloyosu.tfgandroid.modelos.Carrera;
import com.example.pablloyosu.tfgandroid.modelos.FinalizarCarreraDto;
import com.example.pablloyosu.tfgandroid.modelos.UsuarioBiciDto;
import com.google.gson.Gson;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;

import static com.example.pablloyosu.tfgandroid.base.Constantes.URL_SPRING;

//Tearea carreras
public class CarrerasTask extends AsyncTask<Void, Void, Carrera>{
    private UsuarioBiciDto dto;
    private FinalizarCarreraDto dtO;
    private OnTaskCompleteListener listener;
    private Carrera carrera;

    public CarrerasTask(UsuarioBiciDto dto, FinalizarCarreraDto dtO, OnTaskCompleteListener listener) {
        this.dto = dto;
        this.dtO = dtO;
        this.listener = listener;
    }

    @Override
    protected Carrera doInBackground(Void... voids) {
        try {
            String tipo;
            String endPoint = "/carreras";
            if(dtO == null && dto !=null){
                tipo = "POST";
                URL url = new URL(URL_SPRING + endPoint);
                HttpURLConnection urlConnection ;
                urlConnection = ((HttpURLConnection) url.openConnection());
                urlConnection.setRequestMethod(tipo);
                urlConnection.setDoOutput(true);
                urlConnection.setRequestProperty("Content-Type","application/json");

                Gson gson = new Gson();


                String plainJson = gson.toJson(dto);
                String sendOb = plainJson;
                urlConnection.setChunkedStreamingMode(0);
                System.out.println("plainjson->"+sendOb);
                OutputStream outputStream = new BufferedOutputStream(urlConnection.getOutputStream());

                outputStream.write(sendOb.getBytes());
                outputStream.flush();
                outputStream.close();


                InputStream in = new BufferedInputStream(urlConnection.getInputStream());
                String linea = Util.getStringFromInputStream(in);
                System.out.println("______|||||||_________\n"+linea+"\n---------|||||||------------");

                carrera = gson.fromJson(Util.fromJson(linea), Carrera.class);;
                System.out.println("_______________\n"+carrera.toString()+"\n---------------------");
                Sesion.carrera = carrera;
                if(Sesion.carrera != null)
                    System.out.println("_____SESION__________\n"+carrera.toString()+"\n---------------------");

                return carrera;
            }else if(dtO != null && dto ==null) {
                tipo = "PUT";
                URL url = new URL(URL_SPRING + endPoint);
                HttpURLConnection urlConnection ;
                urlConnection = ((HttpURLConnection) url.openConnection());
                urlConnection.setRequestMethod(tipo);
                urlConnection.setDoOutput(true);
                urlConnection.setRequestProperty("Content-Type","application/json");

                Gson gson = new Gson();


                String plainJson = gson.toJson(dtO);
                String sendOb = plainJson;
                urlConnection.setChunkedStreamingMode(0);
                System.out.println("plainjson->"+sendOb);
                OutputStream outputStream = new BufferedOutputStream(urlConnection.getOutputStream());

                outputStream.write(sendOb.getBytes());
                outputStream.flush();
                outputStream.close();


                InputStream in = new BufferedInputStream(urlConnection.getInputStream());
                String linea = Util.getStringFromInputStream(in);
                System.out.println("_______________\n"+linea+"\n---------------------");

                carrera = gson.fromJson(linea, Carrera.class);
                System.out.println("_______________\n"+carrera.toString()+"\n---------------------");

                return null;

            }


        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (ProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e){
            listener.onTaskCompleted(null);
        }
        return null;
    }

    @Override
    protected void onPostExecute(Carrera carrera) {
        super.onPostExecute(carrera);
        this.listener.onTaskCompleted(carrera);
    }
}
