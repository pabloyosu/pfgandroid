package com.example.pablloyosu.tfgandroid.modelos;

import java.io.Serializable;
import java.util.ArrayList;

//Modelo union estación y bicicleta
public class EstacionBici implements Serializable{
    private int id;
    private String calle;
    private int bicisDisponibles;
    private int capacidad;
    private int anclajesDisponibles;
    private boolean estacionAbierta;
    private Double lat;
    private Double lng;
    private ArrayList<Bicicleta> bicicletas;

    public EstacionBici(){

    }

    public ArrayList<Bicicleta> getBicicletas() {
        return bicicletas;
    }

    public void setBicicletas(ArrayList<Bicicleta> bicicletas) {
        this.bicicletas = bicicletas;
    }

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLng() {
        return lng;
    }

    public void setLng(Double lng) {
        this.lng = lng;
    }

    public EstacionBici(String calle, int bicisDisponibles, boolean estacionAbierta, Double lat, Double lng) {

        this.calle = calle;
        this.bicisDisponibles = bicisDisponibles;
        this.estacionAbierta = estacionAbierta;
        this.lat = lat;
        this.lng = lng;
    }

    public String getCalle() {
        return calle;
    }

    public void setCalle(String calle) {
        this.calle = calle;
    }

    public int getBicisDisponibles() {
        return capacidad - anclajesDisponibles;
    }

    public void setBicisDisponibles(int bicisDisponibles) {
        this.bicisDisponibles = bicisDisponibles;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getAnclajesDisponibles() {
        return anclajesDisponibles;
    }

    public void setAnclajesDisponibles(int anclajesDisponibles) {
        this.anclajesDisponibles = anclajesDisponibles;
    }

    public int getCapacidad() {
        return capacidad;
    }

    public void setCapacidad(int capacidad) {
        this.capacidad = capacidad;
    }

    public boolean isEstacionAbierta() {
        return estacionAbierta;
    }

    public void setEstacionAbierta(boolean estacionAbierta) {
        this.estacionAbierta = estacionAbierta;
    }

    @Override
    public String toString() {
        return "EstacionBici{" +
                "id=" + id +
                ", calle='" + calle + '\'' +
                ", bicisDisponibles=" + bicisDisponibles +
                ", capacidad=" + capacidad +
                ", anclajesDisponibles=" + anclajesDisponibles +
                ", estacionAbierta=" + estacionAbierta +
                ", lat=" + lat +
                ", lng=" + lng +
                '}';
    }

    public String getEstado() {
        if (isEstacionAbierta())
            return "abierta";
        return "cerrado ";

    }

    @Override
    public boolean equals(Object obj) {
        if(obj instanceof EstacionBici){
            EstacionBici e = (EstacionBici) obj;
            return this.getCalle().equalsIgnoreCase(e.getCalle());
        }
        return false;
    }

}
