package com.example.pablloyosu.tfgandroid.task;

import android.os.AsyncTask;

import com.example.pablloyosu.tfgandroid.Interfaces.OnTaskCompleteListener;
import com.example.pablloyosu.tfgandroid.base.Sesion;
import com.example.pablloyosu.tfgandroid.modelos.Usuario;
import com.google.gson.Gson;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;

import static com.example.pablloyosu.tfgandroid.base.Constantes.URL_SPRING;

//Tarea login
public class LoginTask extends AsyncTask<Void, Void, Usuario> {
    private String email;
    private String contrasenia;
    private Usuario usuario = null;
    private OnTaskCompleteListener listener;
    public LoginTask(String email, String contrasenia, Usuario usuario,OnTaskCompleteListener listener) {
        this.email = email;
        this.contrasenia = contrasenia;
        this.usuario=usuario;
        this.listener=listener;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    //endPoint Usuario
    @Override
    protected Usuario doInBackground(Void... voids) {
        try {

            //Conexión
            URL url = new URL(URL_SPRING + "/login");
            HttpURLConnection urlConnection ;
            urlConnection = ((HttpURLConnection) url.openConnection());
            urlConnection.setRequestMethod("POST");
            urlConnection.setDoOutput(true);
            urlConnection.setRequestProperty("Content-Type","application/json");

            //Preparación para comprobarlo por email y contraseña
            Gson gson = new Gson();
            LoginDto dto = new LoginDto();
            dto.contrasenia = contrasenia;
            dto.email = email;
            String comment = gson.toJson(dto);
            urlConnection.setChunkedStreamingMode(0);
            System.out.println(comment);
            OutputStream outputStream = new BufferedOutputStream(urlConnection.getOutputStream());

            outputStream.write(comment.getBytes());
            outputStream.flush();
            outputStream.close();


            InputStream in = new BufferedInputStream(urlConnection.getInputStream());
            String linea =  getStringFromInputStream(in);
            System.out.println("_______________\n"+linea+"\n---------------------");

            usuario = gson.fromJson(linea, Usuario.class);
            System.out.println("_______________\n"+usuario.toString()+"\n---------------------");

            //Se guarda el Usuario en la sesión para recordarlo
            Sesion.usuario = usuario;
            //Prueba para ver que datos se guardan en sesion
            System.out.println("--------> DATOS SESION");
            System.out.println("--------> PASWORD: -----------" + Sesion.usuario.getContrasenia());
            System.out.println("--------> SALDO: ---------" + Sesion.usuario.getSaldo());
            System.out.println("--------> ADMIN: --------" + Sesion.usuario.isAdmin());
            return usuario;
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (ProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e){
            listener.onTaskCompleted(null);
        }
        return null;
    }
     private class LoginDto {
        public String email;
        public String contrasenia;
    }

    public static String getStringFromInputStream(InputStream is) throws IOException { BufferedReader br = new BufferedReader(new
            InputStreamReader(is));
        StringBuilder total = new StringBuilder(); String line;
        while ((line = br.readLine()) != null) {
            total.append(line).append('\n'); }
        return total.toString(); }

    @Override
    protected void onPostExecute(Usuario usuario) {
        super.onPostExecute(usuario);
        listener.onTaskCompleted(usuario);
    }

}
