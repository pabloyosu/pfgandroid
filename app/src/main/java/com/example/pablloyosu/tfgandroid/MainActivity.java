package com.example.pablloyosu.tfgandroid;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.pablloyosu.tfgandroid.Interfaces.OnTaskCompleteListener;
import com.example.pablloyosu.tfgandroid.base.Util;
import com.example.pablloyosu.tfgandroid.harcode.EstacionesFalsas;
import com.example.pablloyosu.tfgandroid.modelos.Usuario;
import com.example.pablloyosu.tfgandroid.task.LoginTask;
import com.example.pablloyosu.tfgandroid.task.UsuariosTask;

//Código del layout Main
public class MainActivity extends AppCompatActivity implements View.OnClickListener, OnTaskCompleteListener {
    private Button btLoginAcceso, btRegistrarse;
    private EditText etEmailAcceso, etPasswordAcceso;
    Intent intent;
    Button button;
    EstacionesFalsas es;

    //Método onCreate, se ejecuta al crearse
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        bindUI();
        addOnClickListener(this);
        es = new EstacionesFalsas();
    }

    //Pone los listener a los botones
    private void addOnClickListener(View.OnClickListener listener) {
        btRegistrarse.setOnClickListener(listener);
        btLoginAcceso.setOnClickListener(listener);
        button.setOnClickListener(listener);
    }

    //Une los elementos visuales con el background
    private void bindUI() {
        btLoginAcceso = findViewById(R.id.btLoginAcceso);
        etEmailAcceso = findViewById(R.id.etEmailAcceso);
        etPasswordAcceso = findViewById(R.id.etPasswordAcceso);
        btRegistrarse = findViewById(R.id.btRegistrarse);
        button = findViewById(R.id.mapa);
    }

    //Acciones botones
    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btLoginAcceso:
                doGoAcceso();
                break;
            case R.id.btRegistrarse:
                doGoRegistrarse();
                break;


            case R.id.mapa:
                //Boton para pruebas, se deja invisible si no se usa
                intent = new Intent(this, Mapa.MainMenu.class);
                System.out.println("-------------------boton prueba");
                startActivity(intent);
                break;
        }
    }

    //Comprobación credenciales acceso
    private void doGoAcceso() {
       if (Util.isValidEmail(String.valueOf(etEmailAcceso.getText())) &&
               Util.isValidPassword(String.valueOf(etPasswordAcceso.getText()))){
           Usuario usuario = null;
          LoginTask task = new LoginTask(etEmailAcceso.getText().toString(), etPasswordAcceso.getText().toString(), null ,this);
           task.execute();


       }else {
           Toast.makeText(this, getResources().getString(R.string.toast_error_loggin),
                   Toast.LENGTH_LONG).show();
       }
    }

    //Ir a la ventana registro
    private void doGoRegistrarse() {
        intent = new Intent(this, Registro.class);
        startActivity(intent);
    }

    //Comprueba si se ha loggeado bien al terminar la tarea
    @Override
    public void onTaskCompleted(Object object) {
        if(object != null &&  object instanceof Usuario){
            intent = new Intent(this, Mapa.MainMenu.class);
            startActivity(intent);

        }else{
            Toast.makeText(this, getResources().getString(R.string.toast_error_loggin),
                    Toast.LENGTH_LONG).show();
        }

    }

    @Override
    public void onTaskCompletedList(Object[] object) {

    }
}
