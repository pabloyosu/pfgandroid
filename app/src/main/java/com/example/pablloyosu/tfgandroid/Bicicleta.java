package com.example.pablloyosu.tfgandroid;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.SimpleAdapter;
import android.widget.SimpleCursorAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.pablloyosu.tfgandroid.Interfaces.OnTaskCompleteListener;
import com.example.pablloyosu.tfgandroid.base.EstacionesAdapter;
import com.example.pablloyosu.tfgandroid.base.Sesion;
import com.example.pablloyosu.tfgandroid.base.TareaDescarga;
import com.example.pablloyosu.tfgandroid.modelos.EstacionBici;
import com.example.pablloyosu.tfgandroid.modelos.LatLng;
import com.example.pablloyosu.tfgandroid.modelos.Usuario;
import com.example.pablloyosu.tfgandroid.task.BicicletasCrearTask;
import com.example.pablloyosu.tfgandroid.task.EstacionGetTask;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

//Código del layout bicicleta
public class Bicicleta extends AppCompatActivity implements View.OnClickListener , AdapterView.OnItemSelectedListener, SeekBar.OnSeekBarChangeListener, OnTaskCompleteListener{
    Intent intent;
    private Spinner spEstado;
    private Spinner spEstacion;
    private SeekBar sbBateria;
    private EditText etLatitud;
    private EditText etLongitud;
    private EditText etUltimoUsuario;
    private Button btOk;
    private Button btCancelar;
    private com.example.pablloyosu.tfgandroid.modelos.Bicicleta bicicleta;
    private boolean isCreation;
    private EstacionBici[] estacionesBicis;
    private SimpleAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bicicleta);
        bindUI();

        //Listener
        setOnItemClick(this);
        setOnItemSelectedListener(this);
        setOnSeekBarChangeListener(this);
        intent = getIntent();

        //Determinar opciones que vamos a hacer segun el intent que nos llegue
        if(intent.getBooleanExtra("CREAR", false)){
            isCreation = true;

        }else if(intent.getBooleanExtra("UPDATE", false)){
            isCreation = false;
        }else if(intent.getBooleanExtra("DELETE", false)){
            isCreation = false;
        }
        if(intent.getSerializableExtra("object") !=null){
            bicicleta = (com.example.pablloyosu.tfgandroid.modelos.Bicicleta) intent.
                    getSerializableExtra("object");
            if(intent.getDoubleArrayExtra("COORDS")!= null){
                double[] coord = intent.getDoubleArrayExtra("COORDS");
                System.out.println("----------------" + coord[0]);
                System.out.println("----------------" + coord[1]);
                bicicleta.setLat(coord[0]);
                bicicleta.setLng(coord[1]);
            }
            setParams(isCreation);
        }

        EstacionGetTask task = new EstacionGetTask(this);
        task.execute();

    }

    private void setOnItemSelectedListener(AdapterView.OnItemSelectedListener listener) {
        spEstado.setOnItemSelectedListener(listener);
        spEstacion.setOnItemSelectedListener(listener);
    }

    //Rellenar modelo bicicleta con los datos del layout
    private void getParams() {
        String value = spEstacion.getSelectedItem().toString();
        int estadionId = Integer.parseInt(value.split(" ")[0]);
        System.out.println(value+ " "+estadionId);
        EstacionBici e = new EstacionBici();
        e.setId(estadionId);
        bicicleta.setEstacion(e);
        bicicleta.setLat(Double.parseDouble(String.valueOf(etLatitud.getText())));
        bicicleta.setLng(Double.parseDouble(String.valueOf(etLongitud.getText())));
        bicicleta.setEstadoEnum(spEstado.getSelectedItem().toString());
        System.out.println(" --------------  SPESTADO ----------------> " + spEstado.getSelectedItem().toString());
        bicicleta.setBateria(sbBateria.getProgress());
        BicicletasCrearTask task;
        if(isCreation){
            task = new BicicletasCrearTask(bicicleta, this, "POST");
            task.execute();
        }else {
            System.out.println("----------------PUT----------");
            task = new BicicletasCrearTask(bicicleta, this, "PUT");
            task.execute();
        }

    }

    //Rellenar el layout con los datos del modelo
    private void setParams(boolean isCreation) {
        System.out.println("----------------------> setParams Bicicleta");
        if(isCreation){
            etLatitud.setText(bicicleta.getLat().toString());
            etLongitud.setText(bicicleta.getLng().toString());
        }else {
            sbBateria.setProgress(bicicleta.getBateria());
            etLatitud.setText(bicicleta.getLat().toString());
            etLongitud.setText(bicicleta.getLng().toString());
            spEstado.setSelection(getPosicionSpinner(bicicleta.getEstadoEnum()));
            if(bicicleta.getListaUsuarios() !=null){
                Usuario usuario;
                usuario = bicicleta.getListaUsuarios().get(bicicleta.getListaUsuarios().size());
                etUltimoUsuario.setText(usuario.getId());
            }else {
                Usuario usuario = new Usuario();
                usuario.setNombre("tefgdgsdf");
                usuario.setApellidos("Apellido");




            }
        }
    }

    //Listeners
    private void setOnItemClick(View.OnClickListener listener) {
        btOk.setOnClickListener(listener);
        btCancelar.setOnClickListener(listener);
    }

    private void setOnSeekBarChangeListener(SeekBar.OnSeekBarChangeListener listener){
        sbBateria.setOnSeekBarChangeListener(listener);
    }

    private void bindUI() {
        spEstado = findViewById(R.id.spEstado);
        spEstacion = findViewById(R.id.spEstaciones);
        sbBateria = findViewById(R.id.sbBateria);
        etLatitud = findViewById(R.id.etLatitudd);
        etLongitud = findViewById(R.id.etLongitudd);
        btOk = findViewById(R.id.btOk);
        btCancelar = findViewById(R.id.btCancelar);
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btOk:
                if(Sesion.usuario.isAdmin()){
                    getParams();
                    System.out.println(bicicleta.toString());

                }else {
                    Toast.makeText(this, "Tu Usuario no tiene privilegios para modificar datos",
                            Toast.LENGTH_LONG).show();
                    Intent intent = new Intent(this, Mapa.MainMenu.class);
                    startActivity(intent);
                }



                break;
            case R.id.btCancelar:
                Intent intent = new Intent(this, Mapa.MainMenu.class);
                startActivity(intent);
                break;
        }

    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        final int posicion = i;
        switch (view.getId()){
            default:
                System.out.println("|----------------------------------------------------------");
                System.out.println(view.getId());
                System.out.println("i------>" + i + "    l------>" + l);
                System.out.println(adapterView.getItemAtPosition(i));
                System.out.println("|----------------------------------------------------------");

                break;
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int i, boolean b) {

    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onTaskCompleted(Object object) {
        if(object != null && object instanceof com.example.pablloyosu.tfgandroid.modelos.Bicicleta){
            Toast.makeText(this, "Bicicleta creada", Toast.LENGTH_LONG);
            Intent intent = new Intent(this, Mapa.MainMenu.class);
            startActivity(intent);
        }else {
            Toast.makeText(this, "Error", Toast.LENGTH_LONG);
        }
    }

    @Override
    public void onTaskCompletedList(Object[] object) {
        if(object != null && object instanceof EstacionBici[]){
            try {
                estacionesBicis = (EstacionBici[]) object;
                List<String> list = new ArrayList<>();
                int pos=0;
                int index = 0;
                for (EstacionBici e:  estacionesBicis){
                    pos++;
                    list.add(""+e.getId()+" " + e.getCalle());
                    if(bicicleta != null && bicicleta.getEstacion() != null &&
                            bicicleta.getEstacion().getId()== e.getId()){
                        index = pos;

                    }
                }
                ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                        android.R.layout.simple_spinner_item, list);
                dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spEstacion.setAdapter(dataAdapter);
                spEstacion.setSelection(index - 1);


            }catch (Exception e){

            }
        }
    }
    private int getPosicionSpinner (String estado){
        switch (estado){
            case "AVERIADA":
                return 0;
            case "PERDIDA":
                return 1;
            case "DISPONIBLE":
                return 2;
            case "POCA BATERÍA":
                return 3;
                default:
                    return 0;
        }
    }
}
