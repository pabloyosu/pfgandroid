package com.example.pablloyosu.tfgandroid.modelos;

import com.example.pablloyosu.tfgandroid.R;

import java.io.Serializable;
import java.util.ArrayList;

//Modelo bicicleta
public class Bicicleta  implements Serializable{
    private int id;
    private boolean estado;
    private int bateria;
    private ArrayList<Usuario> listaUsuarios;
    private Double lat;
    private Double lng;
    private EstacionBici estacion;


    private String estadoEnum;

    public Bicicleta() {

    }


    public EstacionBici getEstacion() {
        return estacion;
    }

    public void setEstacion(EstacionBici estacion) {
        this.estacion = estacion;
    }

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLng() {
        return lng;
    }

    public void setLng(Double lng) {
        this.lng = lng;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isEstado() {
        return estado;
    }

    public void setEstado(boolean estado) {
        this.estado = estado;
    }

    public int getBateria() {
        return bateria;
    }

    public void setBateria(int bateria) {
        this.bateria = bateria;
    }

    public ArrayList<Usuario> getListaUsuarios() {
        return listaUsuarios;
    }

    public void setListaUsuarios(ArrayList<Usuario> listaUsuarios) {
        this.listaUsuarios = listaUsuarios;
    }

    public String getEstadoEnum() {
        return estadoEnum;
    }

    public void setEstadoEnum(String estadoEnum) {
        this.estadoEnum = estadoEnum;
    }

    public String getEstado() {
        if (isEstado())
            return String.valueOf(R.string.bici_libre);// TODO crear valor en string
        return String.valueOf(R.string.bici_ocupada);

    }

    @Override
    public String toString() {
        return "Bicicleta{" +
                "id=" + id +
                ", estado=" + estado +
                ", bateria=" + bateria +
                //", listaUsuarios=" + listaUsuarios +
                ", lat=" + lat +
                ", lng=" + lng +
                ", estadoEnum=" + estadoEnum +
                '}';
    }
}
