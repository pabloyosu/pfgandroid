package com.example.pablloyosu.tfgandroid;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.example.pablloyosu.tfgandroid.Interfaces.OnTaskCompleteListener;
import com.example.pablloyosu.tfgandroid.base.BiciAdapter;
import com.example.pablloyosu.tfgandroid.base.EstacionesAdapter;
import com.example.pablloyosu.tfgandroid.base.Sesion;
import com.example.pablloyosu.tfgandroid.base.TareaDescarga;
import com.example.pablloyosu.tfgandroid.modelos.Bicicleta;
import com.example.pablloyosu.tfgandroid.modelos.Carrera;
import com.example.pablloyosu.tfgandroid.modelos.EstacionBici;
import com.example.pablloyosu.tfgandroid.modelos.FinalizarCarreraDto;
import com.example.pablloyosu.tfgandroid.modelos.UsuarioBiciDto;
import com.example.pablloyosu.tfgandroid.task.BicicletasCrearTask;
import com.example.pablloyosu.tfgandroid.task.BicicletasGetTask;
import com.example.pablloyosu.tfgandroid.task.CarrerasTask;
import com.example.pablloyosu.tfgandroid.task.EstacionCrearTask;
import com.example.pablloyosu.tfgandroid.task.EstacionGetTask;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import static com.example.pablloyosu.tfgandroid.base.Constantes.BICI;

//Código layout listado
public class Listado extends AppCompatActivity implements AdapterView.OnItemClickListener, OnTaskCompleteListener{
    EstacionBici[] estacionesBicis;
    Bicicleta[] bicicletas;
    EstacionesAdapter estacionesAdapter;
    BiciAdapter biciAdapter;
    ListView listView;
    TareaDescarga tareaDescarga;
    boolean isBici;
    Intent intent;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listado);
        binUI();
        intent = getIntent();
        isBici = intent.getBooleanExtra("isBici", true);

        //Rellenar unos datos u otros segun sea bici o estación
        if(isBici){
            BicicletasGetTask task = new BicicletasGetTask(this);
            task.execute();
        }else {
            EstacionGetTask task = new EstacionGetTask(this);
            task.execute();
        }
    }

    private ArrayList<Bicicleta> dummi(){
        ArrayList<Bicicleta> bicicletas = new ArrayList<>();
        for (int i = 0; i < 10; i++){
            Bicicleta b = new Bicicleta();
            b.setBateria(i);
            b.setEstado(true);
            Double lat  = 41.646550367760 + Double.valueOf(i/100f);
            Double lng  = -0.8867784414259807 +Double.valueOf(i/100f);
            System.out.println(lat + " ----------- "+i+" -------------- " + lng + " ----"+ Double.valueOf(i/10f));
            b.setLat(lat);
            b.setLng(lng);
            bicicletas.add(b);
        }

        return bicicletas;
    }

    @Override
    protected void onResume() {
        super.onResume();
        //Tareas de descargar datos
        if(isBici){
            //Al final no se descargan bicis
        }else {
            //Descomentar en caso de querer descargar estaciones
            //tareaDescarga = new TareaDescarga( this, estacionesAdapter);
            //tareaDescarga.execute();
        }
    }

    private void binUI(){
        listView = findViewById(R.id.lvListado);
        listView.setOnItemClickListener(this);
        registerForContextMenu(listView);
    }

    private void dumi(){
        for (int i = 0; i<8; i++){
            EstacionBici e = new EstacionBici();
            e.setBicisDisponibles(i);
            e.setCapacidad(10);
            e.setCalle("calle " + i);
            if (i%2==0) {
                e.setEstacionAbierta(true);
            }
            else {
                e.setEstacionAbierta(false);
            }
            System.out.println(e.getEstado());
        }
    }

    //En este método se recogen los toques en la pantalla dentro del listView
    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

    }

    //En este método se crea el context menu
    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        if(isBici){
            getMenuInflater().inflate(R.menu.context_menu_bicicleta, menu);

        }else {
            getMenuInflater().inflate(R.menu.context_menu_estaciones_bici, menu);
        }
    }

    //En este método se crean las opciones del menu
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if(isBici){
            getMenuInflater().inflate(R.menu.menu_bicicleta, menu);

        }else {
            getMenuInflater().inflate(R.menu.menu_estaciones_bici, menu);
        }
        return true;
    }

    //Escoger un caso según el item de las opciones que se haya pulsado
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.miVerTodoB:
                intent = new Intent(this, Mapa.class);
                ArrayList<Bicicleta> b = new ArrayList<>();
                for (int i = 0; i < bicicletas.length; i++){
                    b.add(bicicletas[i]);
                }
                intent.putExtra("BICICLETAS", b);
                startActivity(intent);
                return true;
            case R.id.miVerTodosE:
                intent = new Intent(this, Mapa.class);
                ArrayList<EstacionBici> eb = new ArrayList<>();
                for (int i = 0; i < estacionesBicis.length; i++){
                    eb.add(estacionesBicis[i]);
                }
                intent.putExtra("ESTACIONES", eb);
                startActivity(intent);
                return true;
            case R.id.miCrearEstacion:
                if(Sesion.usuario.isAdmin()){
                    intent = new Intent(this, Mapa.class);
                    intent.putExtra("CREAR_ESTACION", true);
                    startActivity(intent);

                }else {
                    Toast.makeText(this, "Tu Usuario no tiene privilegios para realizar esta acción ",
                            Toast.LENGTH_LONG).show();
                }
                return true;
            case R.id.miCrearBicicleta:
                if (Sesion.usuario.isAdmin()) {
                    intent = new Intent(this, Mapa.class);
                    intent.putExtra("CREAR_BICICLETA", true);
                    startActivity(intent);

                }else{
                    Toast.makeText(this, "Tu Usuario no tiene privilegios para realizar esta acción ",
                            Toast.LENGTH_LONG).show();

                }
                return true;
        }


        return super.onOptionsItemSelected(item);
    }

    /**
     * dfsddadfasdfas
     * @param item
     * @return
     */

    //Escoger un caso según el item que se haya pulsado
    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info =
                (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        final int posicion = info.position;
        EstacionBici estacionSeleccionada = null;
        Bicicleta bicicletaSeleccionada = null;
        switch (item.getItemId()){

            case R.id.miUbicacionB:
                bicicletaSeleccionada = bicicletas[posicion];
                intent = new Intent(this, Mapa.class);
                intent.putExtra("object", bicicletaSeleccionada);
                startActivity(intent);
                break;
            case R.id.miUbicacionE:
                estacionSeleccionada = estacionesBicis[posicion];
                intent = new Intent(this, Mapa.class);
                intent.putExtra("object", estacionSeleccionada);
                startActivity(intent);
                break;
            case R.id.miBorrarB:
                if (Sesion.usuario.isAdmin()) {
                    BicicletasCrearTask task = new BicicletasCrearTask(bicicletas[posicion], this, "DELETE");
                    task.execute();
                    refresher("BICICLETA");

                }else{
                    Toast.makeText(this, "Tu Usuario no tiene privilegios para realizar esta acción ",
                            Toast.LENGTH_LONG).show();

                }
                break;
            case R.id.miBorrarE:
                if (Sesion.usuario.isAdmin()) {
                    //BicicletasCrearTask task = new BicicletasCrearTask(bicicletas[posicion], this, "DELETE");
                    System.out.println("|-------------------------------|"+ estacionesBicis[posicion].toString());

                    EstacionCrearTask estacionCrearTask = new EstacionCrearTask(estacionesBicis[posicion], this, "DELETE");
                    estacionCrearTask.execute();
                    refresher("ESTACION");

                }else{
                    Toast.makeText(this, "Tu Usuario no tiene privilegios para realizar esta acción ",
                            Toast.LENGTH_LONG).show();

                }
                break;
            case R.id.miIncidenciaB:
                bicicletaSeleccionada = bicicletas[posicion];
                intent = new Intent(this, Incidencia.class);
                intent.putExtra("object", bicicletaSeleccionada);
                startActivity(intent);
                break;
            case R.id.miIncidenciaE:
                estacionSeleccionada = estacionesBicis[posicion];
                intent = new Intent(this, Incidencia.class);
                intent.putExtra("object", estacionSeleccionada);
                startActivity(intent);
                break;
            case R.id.miVerDetallesB:
                bicicletaSeleccionada = bicicletas[posicion];
                intent = new Intent(this, com.example.pablloyosu.tfgandroid.Bicicleta.class);
                intent.putExtra("UPDATE", true);
                intent.putExtra("object", bicicletaSeleccionada);
                startActivity(intent);
                break;
            case R.id.miVerDetallesE:
                estacionSeleccionada = estacionesBicis[posicion];
                intent = new Intent(this, Estacion.class);
                intent.putExtra("UPDATE", true);
                intent.putExtra("object", estacionSeleccionada);
                startActivity(intent);
                break;

            case R.id.miBicicletaAlquilar:
                bicicletaSeleccionada = bicicletas[posicion];
                if(Sesion.carrera == null){
                    if(!bicicletaSeleccionada.getEstadoEnum().equals("DISPONIBLE")){
                        Toast.makeText(this, "Esta bicicleta no se encuentra disponible", Toast.LENGTH_LONG).show();
                        break;
                    }
                    UsuarioBiciDto dto = new UsuarioBiciDto();
                    dto.idBici = bicicletaSeleccionada.getId();
                    dto.idUsuario = Sesion.usuario.getId();
                    CarrerasTask alquilar = new CarrerasTask(dto, null, this);
                    alquilar.execute();
                    bicicletaSeleccionada.setEstado(false);
                }else {
                    Toast.makeText(this, "Ya tienes una bicicleta alquilada", Toast.LENGTH_LONG).show();
                }

                break;
            case R.id.miEstacionDevolver:
                if(Sesion.carrera !=null){
                    estacionSeleccionada = estacionesBicis[posicion];
                    FinalizarCarreraDto dtO = new FinalizarCarreraDto();
                    dtO.idCarrera = Sesion.carrera.getId();
                    dtO.idEstacion = estacionSeleccionada.getId();

                    CarrerasTask devolver = new CarrerasTask(null, dtO, this);
                    devolver.execute();
                    Sesion.carrera = null;
                }else {
                    Toast.makeText(this, "No tienes ninguna bicicleta alquilada", Toast.LENGTH_LONG).show();
                }

                break;

        }



        return super.onContextItemSelected(item);

    }

    //Refrescar
    private void refresher(String tipo){
        if(tipo.equalsIgnoreCase("BICICLETA")){
            BicicletasGetTask task = new BicicletasGetTask(this);
            task.execute();
        }

        if(tipo.equalsIgnoreCase("ESTACION")){
            EstacionGetTask task = new EstacionGetTask(this);
            task.execute();
        }
    }

    //Alquilar bicicletas
    @Override
    public void onTaskCompleted(Object object) {
        if(object !=null && object instanceof Carrera){
            Sesion.carrera = (Carrera) object;
            System.out.println(Sesion.carrera.toString());
            System.out.println("IS CARRERA");
            Toast.makeText(this, "Bicicleta alquilada", Toast.LENGTH_LONG).show();

        }else {
            System.out.println("IS NULL");

        }

    }

    //Rellenar con datos
    @Override
    public void onTaskCompletedList(Object[] object) {
       if(object != null && object instanceof  Bicicleta[]){
           //try{
               System.out.println("----------------------try1");
               Bicicleta bicicletasTemp[] = (Bicicleta[]) object;
               bicicletas =  bicicletasTemp;

               biciAdapter = new BiciAdapter(bicicletas, this);
               listView.setAdapter(biciAdapter);
               System.out.println("----------------------try1fin");

           //} catch (Exception e){ }
       } else if(object != null && object instanceof EstacionBici[]){
           try {
               estacionesBicis = (EstacionBici[]) object;

               if(estacionesBicis.length == 0){
                   TareaDescarga tt = new TareaDescarga(this,estacionesAdapter);
                   tt.execute();
               }
               if(Sesion.ordenacion == "alf"){
                   ArrayList <EstacionBici> estacionesOrdenar = new ArrayList<>();
                   estacionesOrdenar.addAll(Arrays.asList(estacionesBicis));
                   Collections.sort(estacionesOrdenar, new Comparator<EstacionBici>() {
                       @Override
                       public int compare(EstacionBici estacionBici, EstacionBici t1) {
                           return estacionBici.getCalle().compareTo(t1.getCalle());
                       }
                   });
                   estacionesBicis = estacionesOrdenar.toArray(new EstacionBici[0]);

               }
               if(Sesion.ordenacion == "id"){
                   ArrayList <EstacionBici> estacionesOrdenar = new ArrayList<>();
                   estacionesOrdenar.addAll(Arrays.asList(estacionesBicis));
                   Collections.sort(estacionesOrdenar, new Comparator<EstacionBici>() {
                       @Override
                       public int compare(EstacionBici estacionBici, EstacionBici t1) {
                           return new Integer(estacionBici.getId()).compareTo(new Integer(t1.getId()));
                       }
                   });
                   estacionesBicis = estacionesOrdenar.toArray(new EstacionBici[0]);
               }
               estacionesAdapter = new EstacionesAdapter(estacionesBicis, this);
               listView.setAdapter(estacionesAdapter);
           }catch (Exception e){

           }
       }else {
           return;
       }
    }
}
