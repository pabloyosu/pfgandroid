package com.example.pablloyosu.tfgandroid.task;

import android.os.AsyncTask;

import com.example.pablloyosu.tfgandroid.Estacion;
import com.example.pablloyosu.tfgandroid.Interfaces.OnTaskCompleteListener;
import com.example.pablloyosu.tfgandroid.base.Util;
import com.example.pablloyosu.tfgandroid.modelos.Bicicleta;
import com.example.pablloyosu.tfgandroid.modelos.EstacionBici;
import com.google.gson.Gson;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import static com.example.pablloyosu.tfgandroid.base.Constantes.URL_SPRING;

//Tarea obtener estación
public class EstacionGetTask extends AsyncTask<Void, Void, EstacionBici[]> {
    private OnTaskCompleteListener listener;
    private EstacionBici[] estaciones;

    public EstacionGetTask(OnTaskCompleteListener listener) {
        this.listener = listener;
    }

    //endPoint
    @Override
    protected EstacionBici[] doInBackground(Void... voids) {
        try {
            URL url = new URL(URL_SPRING + "/get_estaciones");
            HttpURLConnection urlConnection ;
            urlConnection = ((HttpURLConnection) url.openConnection());
            urlConnection.setRequestProperty("Content-Type","application/json");

            Gson gson = new Gson();


            InputStream in = new BufferedInputStream(urlConnection.getInputStream());
            String linea = Util.getStringFromInputStream(in);
            System.out.println("_______________\n"+linea+"\n---------------------");

            estaciones = gson.fromJson(Util.fromJson(linea), EstacionBici[].class);



            return estaciones;
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;

    }

    @Override
    protected void onPostExecute(EstacionBici[] estaciones) {
        super.onPostExecute(estaciones);
        listener.onTaskCompletedList(estaciones);
    }
}
