package com.example.pablloyosu.tfgandroid.task;

import android.os.AsyncTask;

import com.example.pablloyosu.tfgandroid.Interfaces.OnTaskCompleteListener;
import com.example.pablloyosu.tfgandroid.base.Util;
import com.example.pablloyosu.tfgandroid.modelos.Usuario;
import com.google.gson.Gson;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;

import static com.example.pablloyosu.tfgandroid.base.Constantes.URL_SPRING;

//Tarea registro
public class RegistroTask extends AsyncTask<Void, Void, Usuario> {
    private Usuario usuario;
    private OnTaskCompleteListener listener;
    private String tipo;

    public RegistroTask(Usuario usuario, OnTaskCompleteListener listener, String tipo) {
        this.usuario = usuario;
        this.listener = listener;
        this.tipo = tipo;
    }

    //endPoint crear Usuario
    @Override
    protected Usuario doInBackground(Void... voids) {
        try {
            URL url = new URL(URL_SPRING + "/users");
            HttpURLConnection urlConnection ;
            urlConnection = ((HttpURLConnection) url.openConnection());
            urlConnection.setRequestMethod(tipo);
            urlConnection.setDoOutput(true);
            urlConnection.setRequestProperty("Content-Type","application/json");

            Gson gson = new Gson();


            String comment = gson.toJson(usuario);
            String sendOb = comment;
            urlConnection.setChunkedStreamingMode(0);
            System.out.println(sendOb);
            OutputStream outputStream = new BufferedOutputStream(urlConnection.getOutputStream());

            outputStream.write(sendOb.getBytes());
            outputStream.flush();
            outputStream.close();


            InputStream in = new BufferedInputStream(urlConnection.getInputStream());
            String linea = Util.getStringFromInputStream(in);
            System.out.println("_______________\n"+linea+"\n---------------------");

            usuario = gson.fromJson(linea, Usuario.class);
            System.out.println("_______________\n"+usuario.toString()+"\n---------------------");

            return usuario;
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (ProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e){
            listener.onTaskCompleted(null);
        }
        return null;

    }

    @Override
    protected void onPostExecute(Usuario usuario) {
        super.onPostExecute(usuario);
        listener.onTaskCompleted(usuario);
    }
}
