package com.example.pablloyosu.tfgandroid.harcode;

import com.example.pablloyosu.tfgandroid.modelos.EstacionBici;

import java.util.ArrayList;

//Clase para rellenar estaciones falsas y poblar la base de datos
public class EstacionesFalsas {
    public static ArrayList<Hard> estaciones = new ArrayList<>();
    public ArrayList<Hard> hards;
    public EstacionesFalsas() {
        hards = new ArrayList<>();

        for (int i = 0 ; i<10; i++){
            estaciones.add(new Hard("hola"+i, "adios"+i));
        }
    }
    public class Hard{
        public String nombre;
        public String apellido;

        public Hard(String nombre, String apellido) {
            this.nombre = nombre;
            this.apellido = apellido;
        }

        public String getNombre() {
            return nombre;
        }

        public void setNombre(String nombre) {
            this.nombre = nombre;
        }

        public String getApellido() {
            return apellido;
        }

        public void setApellido(String apellido) {
            this.apellido = apellido;
        }

        @Override
        public String toString() {
            return "Hard{" +
                    "nombre='" + nombre + '\'' +
                    ", apellido='" + apellido + '\'' +
                    '}';
        }
    }
}
