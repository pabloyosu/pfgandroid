package com.example.pablloyosu.tfgandroid;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.pablloyosu.tfgandroid.base.Sesion;
import com.example.pablloyosu.tfgandroid.modelos.Bicicleta;
import com.example.pablloyosu.tfgandroid.modelos.EstacionBici;
import com.mapbox.mapboxsdk.Mapbox;
import com.mapbox.mapboxsdk.annotations.MarkerOptions;
import com.mapbox.mapboxsdk.constants.Style;
import com.mapbox.mapboxsdk.geometry.LatLng;
import com.mapbox.mapboxsdk.maps.MapView;
import com.mapbox.mapboxsdk.maps.MapboxMap;
import com.mapbox.mapboxsdk.maps.OnMapReadyCallback;

import java.util.ArrayList;

//Código layout mapa
public class Mapa extends AppCompatActivity implements OnMapReadyCallback, MapboxMap.OnMapClickListener,
        MapboxMap.OnMapLongClickListener {
    private MapView mapView;
    private ArrayList<EstacionBici> estacionBicis;
    private ArrayList<Bicicleta> bicicletas;
    private boolean crearEstacion;
    private boolean crearBicicleta;
    Intent intent;
    EstacionBici estacionBici;
    Bicicleta bicicleta;
    MapboxMap mapbox;
    public static final String DARK = Style.DARK;
    public static final String OUTDOORS = Style.OUTDOORS;
    public static final String LIGTH = Style.LIGHT;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Key de mapbox
        Mapbox.getInstance(this, "pk.eyJ1IjoicGFibG95b3N1IiwiYSI6ImNqYTliZHN0NjBoc2Myd2xvenBvYTdqeXMifQ.RJXZ8ekeAxketTXPjQkrYA");

        setContentView(R.layout.activity_mapa);
        mapView = (MapView) findViewById(R.id.mapView);
        mapView.getMapAsync(this);
        mapView.onCreate(savedInstanceState);
        //Cargar aspecto almacenado en sesion
        mapView.setStyleUrl(Sesion.aspecto);
        intent = getIntent();

        //Escoger los datos a visualizar según la opción
        if(intent.getSerializableExtra("object") != null){
            Object object = intent.getSerializableExtra("object");
            if(object instanceof Bicicleta){
                bicicleta = (Bicicleta) object;
            }else {
                estacionBici = (EstacionBici) object;
            }
        }else if(intent.getSerializableExtra("ESTACIONES") !=null){
            estacionBicis = (ArrayList<EstacionBici>) intent.getSerializableExtra("ESTACIONES");
        }else if(intent.getBooleanExtra("CREAR_ESTACION", false)){
            crearEstacion = intent.getBooleanExtra("CREAR_ESTACION", true);
        }else if(intent.getSerializableExtra("BICICLETAS") !=null){
            bicicletas = (ArrayList<Bicicleta>) intent.getSerializableExtra("BICICLETAS");
            for (Bicicleta b : bicicletas){
                System.out.println(b.toString());
            }
        }else if(intent.getBooleanExtra("CREAR_BICICLETA", false)){
            crearBicicleta = intent.getBooleanExtra("CREAR_ESTACION", true);
        }

    }

    @Override
    protected void onResume() {
        mapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(MapboxMap mapboxMap) {

            }
        });
        super.onResume();
        //Pintar bicicletas o estaciones
        if(bicicleta != null){

                if(bicicleta.getLat() != null && bicicleta.getLng() != null){
                    mapView.getMapAsync(new OnMapReadyCallback() {
                        @Override
                        public void onMapReady(MapboxMap mapboxMap) {
                            mapboxMap.addMarker(new MarkerOptions()
                                    .position(new LatLng(bicicleta.getLat(), bicicleta.getLng()))
                                    .title("Bicicleta")
                                    .snippet(" ")
                            );

                        }
                    });
                }

        }else if(estacionBici !=null){
                if(estacionBici.getLat() !=null && estacionBici.getLng() != null){
                    mapView.getMapAsync(new OnMapReadyCallback() {
                        @Override
                        public void onMapReady(MapboxMap mapboxMap) {
                            mapboxMap.addMarker(new MarkerOptions()
                                    .position(new LatLng(estacionBici.getLat(), estacionBici.getLng()))
                                    .title(estacionBici.getCalle())
                                    .snippet(" ")
                            );
                        }
                    });
                }

            }else if(estacionBicis !=null){
                for(EstacionBici e: estacionBicis){
                    mapView.getMapAsync(new OnMapReadyCallback() {
                        @Override
                        public void onMapReady(MapboxMap mapboxMap) {
                            mapboxMap.addMarker(new MarkerOptions()
                                    .position(new LatLng(e.getLat(), e.getLng()))
                                    .title(e.getCalle())
                                    .snippet("Calle de la estación")
                            );
                        }
                    });
                }

            }else if(bicicletas !=null){
            for(Bicicleta b: bicicletas){
                mapView.getMapAsync(new OnMapReadyCallback() {
                    @Override
                    public void onMapReady(MapboxMap mapboxMap) {
                        mapboxMap.addMarker(new MarkerOptions()
                                .position(new LatLng(b.getLat(), b.getLng()))
                                .title("" + b.getBateria())
                                .snippet("Batería de la bicicleta")
                        );
                    }
                });
            }

        }

        //Cologar aspecto guardado
        mapView.setStyleUrl(Sesion.aspecto);

    }

    //Pulsaciones largas al crear un objeto
    @Override
    public void onMapLongClick(@NonNull LatLng point) {

        System.out.println("Pulsación larga --------------------------------------><>>>>>");
        Intent intent = new Intent(this,Incidencia.class);
        double [] latLong = new double[2];
        latLong[0]=point.getLatitude();
        latLong[1]=point.getLongitude();
        //com.example.pablloyosu.tfgandroid.modelos.LatLng lt = new com.example.pablloyosu.tfgandroid.modelos.LatLng(latLong);
        if(estacionBici !=null){
            intent.putExtra("ESTACION", estacionBici);
            startActivity(intent);
        }else if(bicicleta != null){
            intent.putExtra("BICICLETA", bicicleta);
            startActivity(intent);
        }else if(crearEstacion){
            estacionBici = new EstacionBici();
            estacionBici.setLat(point.getLatitude());
            estacionBici.setLng(point.getLongitude());
            intent = new Intent(this, Estacion.class);
            intent.putExtra("object", estacionBici);
            intent.putExtra("CREAR", true);
            intent.putExtra("COORDS", latLong);

            startActivity(intent);
        }else if(crearBicicleta){
            bicicleta = new Bicicleta();
            bicicleta.setLat(point.getLatitude());
            bicicleta.setLng(point.getLongitude());
            intent = new Intent(this, com.example.pablloyosu.tfgandroid.Bicicleta.class);
            intent.putExtra("object", bicicleta);
            intent.putExtra("CREAR", true);
            intent.putExtra("COORDS", latLong);
            startActivity(intent);
        }

    }


    //Este metodo y el de abaj es el menu que sale con los puntitos en la parte superior de la pantalla
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.context_mapa, menu);
        return true;
    }

    //Opciones del menu opciones
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.miDark:
                Toast.makeText(this, "Puesto en estilo DARK", Toast.LENGTH_LONG).show();
                mapView.setStyleUrl(DARK);
                Sesion.aspecto=DARK;
                return true;
            case R.id.miClear:
                Toast.makeText(this, "Puesto en estilo OUTDOORS", Toast.LENGTH_LONG).show();
                mapView.setStyleUrl(OUTDOORS);
                Sesion.aspecto=OUTDOORS;
                return true;
            case R.id.miLight:
                Toast.makeText(this, "Puesto en estilo LIGTH", Toast.LENGTH_LONG).show();
                mapView.setStyleUrl(LIGTH);
                Sesion.aspecto=LIGTH;
                return true;
            case R.id.miListadoEstaciones:
                intent = new Intent(this, Listado.class);
                intent.putExtra("isBici",false);
                startActivity(intent);
                return true;
            case R.id.miListadoBicis:
                intent = new Intent(this, Listado.class);
                intent.putExtra("isBici",true);
                startActivity(intent);
                return true;
        }


        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onMapClick(@NonNull LatLng point) {

    }

    @Override
    public void onMapReady(MapboxMap mapboxMap) {
        this.mapbox = mapboxMap;
        addListener();
    }
    private void addListener(){
        mapbox.addOnMapClickListener(this);
        mapbox.addOnMapLongClickListener(this);
    }

    public static class MainMenu extends AppCompatActivity implements View.OnClickListener{
        private Button btMapa;
        private Button btEstaciones;
        private Button btBicis;
        private Intent intent;

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_main_menu);
            bindUI();
            setOnClickListener(this);
        }
        @Override
        public boolean onCreateOptionsMenu(Menu menu) {
            getMenuInflater().inflate(R.menu.context_main_menu, menu);
            return true;
        }

        //Opciones del menu opciones
        @Override
        public boolean onOptionsItemSelected(MenuItem item) {
            switch (item.getItemId()){
                case R.id.miMainMenuUsuario:
                    intent = new Intent(this, Usuario.class);
                    startActivity(intent);
                    break;
                case R.id.miMainMenuPreferencias:
                    intent = new Intent(this, Preferencias.class);
                    startActivity(intent);
                    break;

                case R.id.miMainMenuLogOut:
                    Sesion.usuario = null;
                    intent = new Intent(this, MainActivity.class);
                    startActivity(intent);
                    break;
            }


            return super.onOptionsItemSelected(item);
        }

        private void bindUI(){
            btMapa = findViewById(R.id.btMapa);
            btEstaciones = findViewById(R.id.btEstaciones);
            btBicis = findViewById(R.id.btBicis);
        }

        private void setOnClickListener(View.OnClickListener listener){
            btMapa.setOnClickListener(listener);
            btEstaciones.setOnClickListener(listener);
            btBicis.setOnClickListener(listener);
        }

        //Botones iniciales
        @Override
        public void onClick(View view) {
            switch (view.getId()){
                case R.id.btMapa:
                    intent = new Intent(this, Mapa.class);
                    startActivity(intent);
                    break;
                case R.id.btEstaciones:
                    intent = new Intent(this, Listado.class);
                    intent.putExtra("isBici", false);
                    startActivity(intent);
                    break;
                case R.id.btBicis:
                    intent = new Intent(this, Listado.class);
                    intent.putExtra("isBici", true);
                    startActivity(intent);
                    break;
            }
        }
    }
}
