package com.example.pablloyosu.tfgandroid.task;


import android.os.AsyncTask;

import com.example.pablloyosu.tfgandroid.base.Util;
import com.example.pablloyosu.tfgandroid.modelos.Usuario;
import com.google.gson.Gson;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import static com.example.pablloyosu.tfgandroid.base.Constantes.URL_SPRING;

//Tarea buscar Usuario por ID
public class UsuarioByIdTask extends AsyncTask<Void, Void, Usuario> {
    private int id;

    public UsuarioByIdTask() {
    }

    public UsuarioByIdTask(int id) {
        this.id = id;
    }

    //endPoint
    @Override
    protected Usuario  doInBackground(Void... voids) {
        try {
            URL url = new URL(URL_SPRING + "/users?id=" + id);
            HttpURLConnection urlConnection;
            urlConnection = (HttpURLConnection) url.openConnection();
            Gson gson = new Gson();


            InputStream in = new BufferedInputStream(urlConnection.getInputStream());
            String linea = Util.getStringFromInputStream(in);

            Usuario usuario = gson.fromJson(linea.replace("contrasenia", "password"), Usuario.class);
            return usuario;

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }


}
