package com.example.pablloyosu.tfgandroid.modelos;

import java.io.Serializable;

public class LatLng implements Serializable{
    public double []latLng;

    public LatLng(double[] latLng) {
        this.latLng = latLng;
    }
}
