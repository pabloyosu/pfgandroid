package com.example.pablloyosu.tfgandroid.task;

import android.os.AsyncTask;

import com.example.pablloyosu.tfgandroid.base.Constantes;
import com.example.pablloyosu.tfgandroid.base.Util;
import com.example.pablloyosu.tfgandroid.modelos.Usuario;
import com.google.gson.Gson;

import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import static com.example.pablloyosu.tfgandroid.base.Constantes.URL_SPRING;

public class UsuariosTask extends AsyncTask<Void, Void, Void>{
    private String email;
    private String contrasenia;
    private Usuario usuario;
    private String tipo;


    public UsuariosTask(String email, String contrasenia, Usuario usuario, String tipo){
        this.email = email;
        this.contrasenia = contrasenia;
        this.usuario = usuario;
        this.tipo = tipo;
    }
    // url http://localhost:8080/get_usuario?nombre=pablo&contrasenia=1234
    @Override
    protected Void doInBackground(Void... voids) {
        try {


            URL url = new URL(URL_SPRING + "/users");
            HttpURLConnection urlConnection ;
            urlConnection = ((HttpURLConnection) url.openConnection());
            urlConnection.setRequestMethod("PUT");
            // Activar método POST
            urlConnection.setDoOutput(true);
            urlConnection.setRequestProperty("Content-Type","application/json");

            
            Usuario usuario = new Usuario();
            usuario.setId(7);
            usuario.setNombre("ombre");
            usuario.setApellidos("pellidos");
            usuario.setEmail("mail");
            usuario.setContrasenia("hgmbvbnvmvbnvm");
            usuario.setPaypal("jgkjhgkjh");
            usuario.setSaldo(99);
            Gson gson = new Gson();
            String comment = gson.toJson(usuario).replace("password", "contrasenia");
            urlConnection.setChunkedStreamingMode(0);
            System.out.println(comment);
            OutputStream outputStream = new BufferedOutputStream(urlConnection.getOutputStream());

            outputStream.write(comment.getBytes());
            outputStream.flush();
            outputStream.close();




            InputStream in = new BufferedInputStream(urlConnection.getInputStream());
            String linea = Util.getStringFromInputStream(in);
            System.out.println("_______________\n"+linea+"\n---------------------");

           Usuario usuarios = gson.fromJson(linea, Usuario.class);
           System.out.println("_______________\n"+usuarios.toString()+"\n---------------------");

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }


    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }
}
