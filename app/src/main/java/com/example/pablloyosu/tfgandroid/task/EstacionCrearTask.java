package com.example.pablloyosu.tfgandroid.task;

import android.os.AsyncTask;

import com.example.pablloyosu.tfgandroid.Estacion;
import com.example.pablloyosu.tfgandroid.Interfaces.OnTaskCompleteListener;
import com.example.pablloyosu.tfgandroid.base.Util;
import com.example.pablloyosu.tfgandroid.modelos.EstacionBici;
import com.example.pablloyosu.tfgandroid.modelos.Usuario;
import com.google.gson.Gson;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;

import static com.example.pablloyosu.tfgandroid.base.Constantes.URL_SPRING;

//Tarea estación
public class EstacionCrearTask extends AsyncTask<Void, Void, EstacionBici> {
    private EstacionBici estacion;
    private OnTaskCompleteListener listener;
    private String tipo;

    public EstacionCrearTask(EstacionBici estacion, OnTaskCompleteListener listener, String tipo) {
        this.estacion = estacion;
        this.listener = listener;
        this.tipo = tipo;
    }


    @Override
    protected EstacionBici doInBackground(Void... voids) {
        //se utiliza un endpoint u otro segun la situación
        try {
            String endPoint;
            if (tipo == "DELETE"){
                endPoint= "/estaciones?id="+estacion.getId();
            }else{
                endPoint= "/estaciones";

            }
        URL url = new URL(URL_SPRING + endPoint);
        HttpURLConnection urlConnection ;
        urlConnection = ((HttpURLConnection) url.openConnection());
        urlConnection.setRequestMethod(tipo);
        urlConnection.setDoOutput(true);
        urlConnection.setRequestProperty("Content-Type","application/json");

        Gson gson = new Gson();


        String plainJson = gson.toJson(estacion);
        String sendOb = Util.toJsom(plainJson);
        urlConnection.setChunkedStreamingMode(0);
        System.out.println(sendOb);
        OutputStream outputStream = new BufferedOutputStream(urlConnection.getOutputStream());

        outputStream.write(sendOb.getBytes());
        outputStream.flush();
        outputStream.close();


        InputStream in = new BufferedInputStream(urlConnection.getInputStream());
        String linea = Util.getStringFromInputStream(in);
        System.out.println("_______________\n"+linea+"\n---------------------");

        estacion = gson.fromJson(Util.fromJson(linea), EstacionBici.class);
        System.out.println("_______________\n"+estacion.toString()+"\n---------------------");

        return estacion;
    } catch (MalformedURLException e) {
        e.printStackTrace();
    } catch (ProtocolException e) {
        e.printStackTrace();
    } catch (IOException e) {
        e.printStackTrace();
    } catch (Exception e){
        listener.onTaskCompleted(null);
    }
        return null;
}

    @Override
    protected void onPostExecute(EstacionBici estacion) {
        super.onPostExecute(estacion);
        listener.onTaskCompleted(estacion);
    }

}
