package com.example.pablloyosu.tfgandroid;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.pablloyosu.tfgandroid.Interfaces.OnTaskCompleteListener;
import com.example.pablloyosu.tfgandroid.base.Util;
import com.example.pablloyosu.tfgandroid.modelos.Usuario;
import com.example.pablloyosu.tfgandroid.task.RegistroTask;

//Código del layout Registro
public class Registro extends AppCompatActivity implements View.OnClickListener, OnTaskCompleteListener{
    Button btRegistroRegistro;
    EditText etNombreRegistro, etApellidosRegistro, etEmailRegistro, etContraseniaRegistro,
            etConfirmarContraseniaRegistro, etPaypalRegistro;
    Intent intent;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);
        bindUI();
        addOnClickListener(this);
    }

    private void addOnClickListener(View.OnClickListener listener){
        btRegistroRegistro.setOnClickListener(listener);
    }

    private void bindUI() {
        btRegistroRegistro = findViewById(R.id.btRegistroRegistro);
        etNombreRegistro = findViewById(R.id.etNombreRegistro);
        etApellidosRegistro = findViewById(R.id.etApellidosRegistro);
        etEmailRegistro = findViewById(R.id.etEmailRegistro);
        etContraseniaRegistro = findViewById(R.id.etContraseniaRegistro);
        etConfirmarContraseniaRegistro = findViewById(R.id.etConfirmarContraseniaRegistro);
        etPaypalRegistro = findViewById(R.id.etPaypalRegistro);

    }

    //Mandar los datos de registro
    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btRegistroRegistro:
                //Comrpobar que los datos son válidos
                if(isValidCredetials()){
                    Usuario usuario = new Usuario();
                    usuario.setNombre(String.valueOf(etNombreRegistro.getText()));
                    usuario.setApellidos(String.valueOf(etApellidosRegistro.getText()));
                    usuario.setEmail(etEmailRegistro.getText().toString());
                    usuario.setContrasenia(etContraseniaRegistro.getText().toString());
                    usuario.setPaypal(String.valueOf(etPaypalRegistro.getText()));
                    RegistroTask task = new RegistroTask(usuario, this, "POST");
                    task.execute();
                    System.out.println("-------------------Usuario registrado");
                    intent = new Intent(this, MainActivity.class);
                    startActivity(intent);
                }else {
                    Toast.makeText(this, "Error, revisa los datos", Toast.LENGTH_LONG).show();
                }
                break;
        }
    }

    //Reglas para verificar que los datos sean válidos
    private boolean isValidCredetials() {
        return Util.isValidEmail(String.valueOf(etEmailRegistro.getText())) &&
                etContraseniaRegistro.getText().toString().equals(etConfirmarContraseniaRegistro.getText().toString()) &&
                Util.isValidPassword(String.valueOf(etContraseniaRegistro.getText())) &&
                Util.isValidEmail(String.valueOf(etPaypalRegistro.getText())) &&
                !TextUtils.isEmpty(etNombreRegistro.getText()) &&
                !TextUtils.isEmpty(etApellidosRegistro.getText());
    }

    //Ir al layout Mapa
    @Override
    public void onTaskCompleted(Object object) {
        if(object != null && object instanceof  Usuario){
        Intent intent = new Intent(this, Mapa.MainMenu.class);
        } else {
            Toast.makeText(this, getResources().getString(R.string.toast_error_loggin),
                    Toast.LENGTH_LONG).show();
        }

    }

    @Override
    public void onTaskCompletedList(Object[] object) {

    }
}
