package com.example.pablloyosu.tfgandroid.base;

import com.example.pablloyosu.tfgandroid.modelos.Carrera;
import com.example.pablloyosu.tfgandroid.modelos.Usuario;
import com.mapbox.mapboxsdk.constants.Style;

//Esta clase mantiene los datos del Usuario logeado
public class Sesion {
    public static Usuario usuario;
    public static Carrera carrera;
    public static String aspecto = Style.SATELLITE;
    public static String ordenacion = "alf";

}
